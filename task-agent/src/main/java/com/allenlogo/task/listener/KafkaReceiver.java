package com.allenlogo.task.listener;

import com.allenlogo.task.service.AgentService;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Optional;

/**
 * @author allen logo
 */
@Component
@Slf4j
public class KafkaReceiver {

    @Resource
    private AgentService agentService;

    @KafkaListener(topics = {"taskTopic"})
    public void listen(ConsumerRecord<?, ?> record) {
        Optional<?> kafkaMessage = Optional.ofNullable(record.value());
        if (kafkaMessage.isPresent()) {
            Object message = kafkaMessage.get();
            log.info("----------------- record =" + record);
            log.info("------------------ message =" + message);
        }else {
            log.info("----------------- record =" + record);
        }
    }
}
