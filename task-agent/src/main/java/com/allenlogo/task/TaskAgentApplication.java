package com.allenlogo.task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaskAgentApplication {

    public static void main(String[] args) {
        SpringApplication.run(TaskAgentApplication.class, args);
    }

}

