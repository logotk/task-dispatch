package com.allenlogo.task;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.http.HttpSession;

/**
 * Created with IntelliJ IDEA.
 *
 * @author allenlogo
 * @Date 2019/1/14
 * @Time 22:30
 * To change this template use File | Settings | File Templates.
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TaskManagerApplication.class)
//@ContextConfiguration(locations = "classpath:application.properties")
@Slf4j
public class BaseControllerTest {

    public MockMvc mockMvc;
    @Autowired
    public ObjectMapper objectMapper;
    @Autowired
    private WebApplicationContext wac;
    private MockHttpSession session;

    @Before
    public void setup() {
        //MockMvcBuilders使用构建MockMvc对象   （项目拦截器有效）
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        this.session = new MockHttpSession();
    }

    protected void genRequestBuilderContent(MockHttpServletRequestBuilder rb, Object vo) throws JsonProcessingException {
        String jn = objectMapper.writeValueAsString(vo);
        rb.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).content(jn);
    }

    /**
     * 获取登入信息session
     * @return
     * @throws Exception
     */
    public HttpSession getLoginSession() throws Exception{
        return  session;
    }

    @After
    public void remove() {
    }
}
