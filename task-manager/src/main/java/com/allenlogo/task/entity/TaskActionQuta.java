package com.allenlogo.task.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Table(name = "task_action_quta")
public class TaskActionQuta {
    @Id
    private Integer guid;

    @Column(name = "cron")
    private String cron;

    @Column(name = "volume")
    private Integer volume;

    @Column(name = "succVol")
    private Integer succVol;

    @Column(name = "infoGuid")
    private Integer infoGuid;

    @Column(name = "infoCode")
    private String infoCode;

    @Column(name = "runServerGuid")
    private Integer runServerGuid;

    @Column(name = "execActionGuid")
    private Integer execActionGuid;

    public TaskActionQuta(){

    }

    public TaskActionQuta(Integer guid, String cron,Integer volume, Integer succVol,Integer infoGuid, String infoCode, Integer runServerGuid, Integer execActionGuid) {
        this.guid = guid;
        this.cron = cron;
        this.volume = volume;
        this.succVol = succVol;
        this.infoGuid = infoGuid;
        this.infoCode = infoCode;
        this.runServerGuid = runServerGuid;
        this.execActionGuid = execActionGuid;
    }
}