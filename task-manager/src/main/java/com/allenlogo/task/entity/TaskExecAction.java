package com.allenlogo.task.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Table(name = "task_exec_action")
public class TaskExecAction {
    @Id
    private Integer guid;

    @Column(name = "name")
    private String name;

    @Column(name = "classPath")
    private String classPath;

    @Column(name = "remark")
    private String remark;

    public TaskExecAction(){

    }

    public TaskExecAction(Integer guid, String name, String classPath, String remark) {
        this.guid = guid;
        this.name = name;
        this.classPath = classPath;
        this.remark = remark;
    }
}