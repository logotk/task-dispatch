package com.allenlogo.task.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Table(name = "task_exec_record")
public class TaskExecRecord {
    @Id
    private Integer guid;

    @Column(name = "infoGuid")
    private Integer infoGuid;

    @Column(name = "infoCode")
    private String infoCode;

    @Column(name = "createTime")
    private Date createTime;

    @Column(name = "sendTime")
    private Date sendTime;

    @Column(name = "runServerGuid")
    private Integer runServerGuid;

    @Column(name = "infoScript")
    private String infoScript;

    public TaskExecRecord(){

    }

    public TaskExecRecord(Integer guid, Integer infoGuid, String infoCode, Date createTime, Date sendTime, Integer runServerGuid, String infoScript) {
        this.guid = guid;
        this.infoGuid = infoGuid;
        this.infoCode = infoCode;
        this.createTime = createTime;
        this.sendTime = sendTime;
        this.runServerGuid = runServerGuid;
        this.infoScript = infoScript;
    }
}