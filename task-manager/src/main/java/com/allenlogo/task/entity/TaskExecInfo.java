package com.allenlogo.task.entity;

import lombok.Data;

import java.util.Date;
import javax.persistence.*;

@Table(name = "task_exec_info")
@Data
public class TaskExecInfo {
    /**
     * 主键
     */
    @Id
    private Integer guid;

    /**
     * 接收时间
     */
    @Column(name = "receiveTime")
    private Date receiveTime;

    /**
     * 任务开始执行时间
     */
    @Column(name = "startTime")
    private Date startTime;

    /**
     * 任务结束时间
     */
    @Column(name = "endTime")
    private Date endTime;

    /**
     * 运行状态
     */
    @Column(name = "runStatus")
    private String runStatus;

    /**
     * 执行阶段
     */
    @Column(name = "execStatus")
    private String execStatus;
}