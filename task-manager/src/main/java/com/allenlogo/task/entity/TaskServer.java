package com.allenlogo.task.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Table(name = "task_server")
public class TaskServer {
    @Id
    private Integer guid;

    @Column(name = "name")
    private String name;

    @Column(name = "ip")
    private String ip;

    @Column(name = "port")
    private String port;

    @Column(name = "remark")
    private String remark;

    public TaskServer(){

    }

    public TaskServer(Integer guid, String name, String ip, String port, String remark) {
        this.guid = guid;
        this.name = name;
        this.ip = ip;
        this.port = port;
        this.remark = remark;
    }
}