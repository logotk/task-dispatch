package com.allenlogo.task.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Table(name = "task_info")
public class TaskInfo {
    @Id
    private Integer guid;

    @Column(name = "createTime")
    private Date createTime;

    @Column(name = "updateTime")
    private Date updateTime;

    @Column(name = "isDel")
    private Integer isDel;

    @Column(name = "isUse")
    private Integer isUse;

    @Column(name = "code")
    private String code;

    @Column(name = "name")
    private String name;

    @Column(name = "startDate")
    private String startDate;

    @Column(name = "remark")
    private String remark;

    @Column(name = "periods")
    private String periods;

    @Column(name = "createGuid")
    private Integer createGuid;

    @Column(name = "endDate")
    private String endDate;

    @Column(name = "runScript")
    private String runScript;

    public TaskInfo(){}

    public TaskInfo(Integer guid, Date createTime, Date updateTime, Integer isDel, Integer isUse, String code, String name, String startDate, String remark, String periods, Integer createGuid, String endDate, String runScript) {
        this.guid = guid;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.isDel = isDel;
        this.isUse = isUse;
        this.code = code;
        this.name = name;
        this.startDate = startDate;
        this.remark = remark;
        this.periods = periods;
        this.createGuid = createGuid;
        this.endDate = endDate;
        this.runScript = runScript;
    }
}