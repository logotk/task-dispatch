package com.allenlogo.task;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class TaskManagerApplication {
    public static void main(String[] args){
        new SpringApplicationBuilder(TaskManagerApplication.class).build().run(args);
    }
}
