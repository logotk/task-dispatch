package com.allenlogo.task.service.impl;

import com.allenlogo.task.bo.TaskActionQutaBo;
import com.allenlogo.task.bo.TaskActionTimingBo;
import com.allenlogo.task.entity.TaskInfo;
import com.allenlogo.task.manager.SchedulerManager;
import com.allenlogo.task.manager.TaskInfoManager;
import com.allenlogo.task.service.TaskInfoService;
import com.allenlogo.task.vo.request.TIQutaAddReq;
import com.allenlogo.task.vo.request.TITimingAddReq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created with IntelliJ IDEA.
 *
 * @author allenlogo
 * @Date 2019/1/13
 * @Time 22:42
 * To change this template use File | Settings | File Templates.
 */
@Service("taskInfoService")
public class TaskInfoServiceImpl implements TaskInfoService {
    @Autowired
    private TaskInfoManager taskInfoManager;
    @Autowired
    private SchedulerManager schedulerManager;

    /**
     * 创建任务链-定额任务
     * @param tiQutaAddReq
     */
    @Override
    @Transactional
    public void addTashInfo(TIQutaAddReq tiQutaAddReq) {
        //创建任务信息-定额
        TaskInfo taskInfo = taskInfoManager.createTaskInfo(tiQutaAddReq);
        //创建任务行为
        TaskActionQutaBo taskActionQutaBo = taskInfoManager.createTaskActionQuta(taskInfo,tiQutaAddReq);
        //内存-定额执行器添加任务
        schedulerManager.addTaskActionQutaBo(taskActionQutaBo);
    }

    /**
     * 创建任务链-定时任务
     * @param tiTimingAddReqq
     */
    @Override
    @Transactional
    public void addTashInfo(TITimingAddReq tiTimingAddReqq) {
        //创建任务信息-定时
        TaskInfo taskInfo = taskInfoManager.createTaskInfo(tiTimingAddReqq);
        //创建任务行为
        TaskActionTimingBo taskActionTimingBo = taskInfoManager.createTaskActionQuta(taskInfo,tiTimingAddReqq);
        //内存-定额执行器添加任务
        schedulerManager.addTaskActionTimingBo(taskActionTimingBo);
    }
}
