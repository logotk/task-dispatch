package com.allenlogo.task.config;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import tk.mybatis.spring.annotation.MapperScan;

import javax.sql.DataSource;

/**
 * Created with IntelliJ IDEA.
 *
 * @author allenlogo
 * @Date 2019/2/23
 * @Time 22:28
 * To change this template use File | Settings | File Templates.
 */
@SpringBootConfiguration
@MapperScan(basePackages = "com.allenlogo.task.dao.agent", sqlSessionFactoryRef = "oracleSessionFactory")
public class AgentDbConfig {
    @Autowired
    @Qualifier("agent")
    private DataSource agent;

    @Bean
    public SqlSessionFactory oracleSessionFactory(){
        SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
        bean.setDataSource(agent);
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        try{
            bean.setMapperLocations(resolver.getResources("classpath*:com/allenlogo/task/dao/agent/mappers/*.xml"));
            return bean.getObject();
        }catch(Exception e){
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Bean
    public PlatformTransactionManager oracleTransactionManager() {
        return new DataSourceTransactionManager(agent);
    }
}
