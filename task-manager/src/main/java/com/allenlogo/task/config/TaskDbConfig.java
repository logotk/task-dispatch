package com.allenlogo.task.config;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import tk.mybatis.spring.annotation.MapperScan;

import javax.sql.DataSource;

/**
 * Created with IntelliJ IDEA.
 *
 * @author allenlogo
 * @Date 2019/2/23
 * @Time 22:27
 * To change this template use File | Settings | File Templates.
 */

@SpringBootConfiguration
@MapperScan(basePackages = "com.allenlogo.task.dao.task")
public class TaskDbConfig {

    @Autowired
    @Qualifier("task")
    private DataSource task;

    @Bean
    @Primary
    public SqlSessionFactory mysqlSessionFactory() throws Exception {
        SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
        bean.setDataSource(task);
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        try{
            bean.setMapperLocations(resolver.getResources("classpath*:com/allenlogo/task/dao/task/mappers/*.xml"));
            return bean.getObject();
        }catch(Exception e){
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        //return bean.getObject();
    }

    @Bean
    @Primary
    public PlatformTransactionManager mysqlTransactionManager() {
        return new DataSourceTransactionManager(task);
    }
}
