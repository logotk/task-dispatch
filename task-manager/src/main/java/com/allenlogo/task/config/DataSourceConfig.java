package com.allenlogo.task.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

/**
 * Created with IntelliJ IDEA.
 *
 * @author allenlogo
 * @Date 2019/2/23
 * @Time 22:22
 * To change this template use File | Settings | File Templates.
 */
@Configuration
public class DataSourceConfig {
    /**
     * 默认数据源
     * 将properties中以mysql为前缀的参数值，写入方法返回的对象中
     * @return
     */
    @Bean(name = "task")
    /*@Primary*/
    @ConfigurationProperties(prefix="task")
    public DataSource mysqDataSource() {
        //通过DataSourceBuilder构建数据源
        return DataSourceBuilder.create().type(HikariDataSource.class).build();
    }

    /**
     * 副数据库需要配置@Qualifier
     * @return
     */
    @Bean(name = "agent")
    /*@Qualifier("agent")*/
    @ConfigurationProperties(prefix="agent")
    public DataSource oracleDataSource() {
        return DataSourceBuilder.create().type(HikariDataSource.class).build();
    }
}
