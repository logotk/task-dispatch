package com.allenlogo.task.dao.task;

import com.allenlogo.task.entity.TaskActionQuta;
import tk.mybatis.mapper.common.IdsMapper;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.special.InsertListMapper;


public interface TaskActionQutaMapper extends Mapper<TaskActionQuta>, IdsMapper<TaskActionQuta>, InsertListMapper<TaskActionQuta> {

}