package com.allenlogo.task.dao.task;

import com.allenlogo.task.entity.TaskExecAction;
import tk.mybatis.mapper.additional.insert.InsertListMapper;
import tk.mybatis.mapper.common.IdsMapper;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface TaskExecActionMapper extends Mapper<TaskExecAction>, IdsMapper<TaskExecAction>, InsertListMapper<TaskExecAction> {
}