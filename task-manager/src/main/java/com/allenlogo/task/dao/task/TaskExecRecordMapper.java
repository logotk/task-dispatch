package com.allenlogo.task.dao.task;

import com.allenlogo.task.entity.TaskExecRecord;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.IdsMapper;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.base.insert.InsertMapper;

public interface TaskExecRecordMapper extends Mapper<TaskExecRecord>, IdsMapper<TaskExecRecord>, InsertMapper<TaskExecRecord> {
    /**
     * 输入执行阶段，记录计数
     * @param runStatus 执行阶段
     * @return
     */
    int countRecordByRunStatus(@Param("runStatus")String runStatus);
}