package com.allenlogo.task.dao.agent;

import com.allenlogo.task.entity.TaskExecInfo;
import tk.mybatis.mapper.common.IdsMapper;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.special.InsertListMapper;

public interface TaskExecInfoMapper extends Mapper<TaskExecInfo>, IdsMapper<TaskExecInfo>, InsertListMapper<TaskExecInfo> {
}