package com.allenlogo.task.dao.task;

import com.allenlogo.task.bo.TaskBo;
import com.allenlogo.task.entity.TaskInfo;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.IdsMapper;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.base.insert.InsertMapper;

import java.util.List;

public interface TaskInfoMapper extends Mapper<TaskInfo>, IdsMapper<TaskInfo>, InsertMapper<TaskInfo> {

    TaskInfo selectByGuid(@Param("guid")Integer guid);

    List<TaskBo> getTaskTimingBo();

    List<TaskBo> getTaskQutaBo();
}