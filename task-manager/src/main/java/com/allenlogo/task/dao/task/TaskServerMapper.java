package com.allenlogo.task.dao.task;

import com.allenlogo.task.entity.TaskServer;
import tk.mybatis.mapper.common.IdsMapper;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.base.insert.InsertMapper;

public interface TaskServerMapper extends Mapper<TaskServer>, IdsMapper<TaskServer>, InsertMapper<TaskServer> {
}