package com.allenlogo.task.bo;

import com.allenlogo.task.entity.TaskActionTiming;
import com.allenlogo.task.entity.TaskExecAction;
import com.allenlogo.task.entity.TaskInfo;
import com.allenlogo.task.entity.TaskServer;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 *
 * @author allenlogo
 * @Date 2019/1/16
 * @Time 22:29
 * To change this template use File | Settings | File Templates.
 */
@Data
public class TaskActionTimingBo {
    private TaskActionTiming taskActionTiming;
    private TaskExecAction taskExecAction;
    private TaskServer taskServer;
    private TaskInfo taskInfo;
}
