package com.allenlogo.task.bo;

import com.allenlogo.task.entity.*;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 *
 * @author allenlogo
 * @Date 2019/1/16
 * @Time 22:32
 * To change this template use File | Settings | File Templates.
 */
@Data
public class TaskActionQutaBo {
    private TaskActionQuta taskActionQuta;
    private TaskExecAction taskExecAction;
    private TaskServer taskServer;
    private TaskInfo taskInfo;
}
