package com.allenlogo.task.bo;

import com.allenlogo.task.entity.TaskServer;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 *
 * @author allenlogo
 * @Date 2019/1/17
 * @Time 23:03
 * To change this template use File | Settings | File Templates.
 */
@Data
public class TaskBo {
    private Integer guid;
    private String code;
    private String name;
    private String startDate;
    private String endDate;
    private String periods;
    private String runScript;
    private String cron;
    private String classPath;
    private TaskServer taskServer;
}
