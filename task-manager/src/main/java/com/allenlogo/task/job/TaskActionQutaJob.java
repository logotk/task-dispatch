package com.allenlogo.task.job;

import com.alibaba.fastjson.JSONObject;
import com.allenlogo.task.bo.TaskBo;
import com.allenlogo.task.constant.TaskConstant;
import com.allenlogo.task.entity.TaskExecRecord;
import com.allenlogo.task.manager.TaskManager;
import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author allenlogo
 * @Date 2019/1/19
 * @Time 21:28
 * To change this template use File | Settings | File Templates.
 */
@Slf4j
public class TaskActionQutaJob implements Job {

    @Autowired
    private TaskManager taskManager;
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        List<TaskBo> taskBos = ((List<TaskBo>)context.getMergedJobDataMap().get(TaskConstant.JOB_DATA_MAP_TASKBO));
        for(TaskBo taskBo : taskBos){
            log.info("TaskActionQutaJob -> {}",JSONObject.toJSONString(taskBo));
            TaskExecRecord taskExecRecord = taskManager.createRecord(taskBo);
            taskManager.sendTask(taskExecRecord);
        }
    }
}
