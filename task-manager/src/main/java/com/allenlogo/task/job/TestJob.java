package com.allenlogo.task.job;

import com.allenlogo.task.constant.TaskConstant;
import com.allenlogo.task.dao.task.TaskExecRecordMapper;
import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public class TestJob implements Job {
    @Autowired
    private TaskExecRecordMapper taskExecRecordMapper;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        log.info("TestJob -> {}",jobExecutionContext.getMergedJobDataMap().get(TaskConstant.JOB_DATA_MAP_TASKBO));
    }
}
