package com.allenlogo.task.manager;

import com.alibaba.fastjson.JSONObject;
import com.allenlogo.task.bo.TaskBo;
import com.allenlogo.task.dao.agent.TaskExecInfoMapper;
import com.allenlogo.task.dao.task.TaskExecRecordMapper;
import com.allenlogo.task.dao.task.TaskInfoMapper;
import com.allenlogo.task.entity.TaskExecInfo;
import com.allenlogo.task.entity.TaskExecRecord;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author allenlogo
 * @Date 2019/1/19
 * @Time 22:01
 * To change this template use File | Settings | File Templates.
 */
@Component
@Slf4j
public class TaskManager {

    @Autowired
    private SchedulerManager schedulerManager;
    @Autowired
    private TaskInfoMapper taskInfoMapper;
    @Autowired
    private TaskExecInfoMapper taskExecInfoMapper;
    @Autowired
    private TaskExecRecordMapper taskExecRecordMapper;

    @Autowired
    private KFKManager kfkManager;
    /**
     * 初始化
     * 将数据库游戏有效的任务加入内存执行器
     */
    @PostConstruct
    public void init() throws InterruptedException {
        // 定时任务初始化
        int waitSec = 30;
        int repeatFreq = 10;
        log.info("任务初始化开始");
        while (true){
            int rowIndex = taskExecRecordMapper.countRecordByRunStatus("0");
            if(rowIndex==0){
                log.info("任务初始化中");
                intit_task();
                break;
            }else {
                repeatFreq = repeatFreq-1;
                Thread.sleep(waitSec*1000);
                //Time.sleep(waitSec);
            }
            if(repeatFreq==0){
                log.info("任务初始化失败");
                break;
            }
        }
        log.info("任务初始化结束");
    }

    public void intit_task(){
        schedulerManager.clear();
        List<TaskBo> taskBoTimingList = taskInfoMapper.getTaskTimingBo();
        if(taskBoTimingList != null){
            schedulerManager.initTaskBo(taskBoTimingList);
        }
        List<TaskBo> taskBoQutaList = taskInfoMapper.getTaskQutaBo();
        if(taskBoTimingList != null){
            schedulerManager.initTaskBo(taskBoQutaList);
        }
    }

    public TaskExecRecord createRecord(TaskBo taskBo){
        TaskExecRecord taskExecRecord = new TaskExecRecord();
        // task info
        taskExecRecord.setInfoCode(taskBo.getCode());
        taskExecRecord.setInfoGuid(taskBo.getGuid());
        taskExecRecord.setInfoScript(taskBo.getRunScript());
        taskExecRecord.setRunServerGuid(taskBo.getTaskServer().getGuid());
        // exec info
        taskExecRecord.setCreateTime(new Date());
        taskExecRecord.setSendTime(new Date());
        taskExecRecordMapper.insert(taskExecRecord);
        return taskExecRecord;
    }

    public void createErrorExecInfo(String messageJsonStr){
        TaskExecRecord taskExecRecord = JSONObject.parseObject(messageJsonStr,TaskExecRecord.class);
        TaskExecInfo taskExecInfo = new TaskExecInfo();
        taskExecInfo.setGuid(taskExecRecord.getGuid());
        taskExecInfo.setEndTime(new Date());
        taskExecInfo.setRunStatus("1");
        taskExecInfo.setExecStatus("1");
        taskExecInfoMapper.insert(taskExecInfo);
    }

    public void sendTask(TaskExecRecord taskExecRecord){
        kfkManager.sendMessage(taskExecRecord.getRunServerGuid(), JSONObject.toJSONString(taskExecRecord));
    }

}
