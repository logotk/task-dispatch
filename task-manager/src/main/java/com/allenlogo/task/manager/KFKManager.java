package com.allenlogo.task.manager;

import com.alibaba.fastjson.JSONObject;
import com.allenlogo.task.bo.Message;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;

import java.util.Date;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 *
 * @author allenlogo
 * @Date 2019/1/16
 * @Time 22:26
 * To change this template use File | Settings | File Templates.
 */
@Component
@Slf4j
public class KFKManager {

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;
    @Autowired
    private TaskManager taskManager;

    public void sendMessage(){
        String messageJsonStr = createMessageJsonStr(null,UUID.randomUUID().toString());
        log.info("+++++++++++++++++++++  message = {}", messageJsonStr);
        sendMessageBase("taskTopic",messageJsonStr);
    }

    public void sendMessage(Integer serverGuid,String taskBo){
        String messageJsonStr = createMessageJsonStr(serverGuid,taskBo);
        log.info("+++++++++++++++++++++  message = {}", messageJsonStr);
        sendMessageBase("taskTopic",messageJsonStr);
    }

    public String createMessageJsonStr(Integer serverGuid,String msg){
        Message message = new Message();
        message.setId(System.currentTimeMillis());
        message.setMsg(msg);
        message.setServerId(serverGuid);
        message.setSendTime(new Date());
        String messageJsonStr = JSONObject.toJSONString(message);
        return messageJsonStr;
    }

    /**
     * 发送kafka消息
     * 基础
     * 回调函数记录发送结果
     * @param topic
     * @param messageJsonStr
     */
    public void sendMessageBase(String topic,String messageJsonStr){
        ListenableFuture future = kafkaTemplate.send(topic, messageJsonStr);
        future.addCallback(
                o -> {log.info("send success -> {}" , messageJsonStr);},
                throwable -> {log.error("{} send error -> Exception {}" ,messageJsonStr,throwable.getMessage());taskManager.createErrorExecInfo(messageJsonStr);}
        );
    }

}
