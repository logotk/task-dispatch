package com.allenlogo.task.manager;

import com.allenlogo.task.bo.TaskActionQutaBo;
import com.allenlogo.task.bo.TaskActionTimingBo;
import com.allenlogo.task.constant.TaskConstant;
import com.allenlogo.task.dao.task.*;
import com.allenlogo.task.entity.*;
import com.allenlogo.task.exception.BusinessException;
import com.allenlogo.task.exception.ExceptionTypeEnum;
import com.allenlogo.task.util.UUIDUtil;
import com.allenlogo.task.vo.request.TIQutaAddReq;
import com.allenlogo.task.vo.request.TITimingAddReq;
import com.allenlogo.task.vo.request.TaskInfoAddReq;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 *
 * @author allenlogo
 * @Date 2019/1/14
 * @Time 20:24
 * To change this template use File | Settings | File Templates.
 */
@Component
@Slf4j
public class TaskInfoManager {

    @Autowired
    private OrikaBeanMapper orikaBeanMapper;

    @Autowired
    private TaskInfoMapper taskInfoMapper;
    @Autowired
    private TaskExecActionMapper taskExecActionMapper;
    @Autowired
    private TaskServerMapper taskServerMapper;
    @Autowired
    private TaskActionTimingMapper taskActionTimingMapper;
    @Autowired
    private TaskActionQutaMapper taskActionQutaMapper;

    public void getList(){
        taskActionQutaMapper.selectAll();
    }

    /**
     * 创建任务信息
     * @param taskInfoAddReq
     * @return
     */
    public TaskInfo createTaskInfo(TaskInfoAddReq taskInfoAddReq){
        //创建任务信息-定额
        TaskInfo taskInfo = orikaBeanMapper.map(taskInfoAddReq,TaskInfo.class);
        taskInfo.setCreateTime(new Date());
        taskInfo.setIsDel(TaskConstant.IS_DEL_ON);
        taskInfo.setIsUse(TaskConstant.IS_USE_ON);
        taskInfo.setCode(UUIDUtil.get12UUID());
        taskInfo.setCreateGuid(TaskConstant.USER_GUID);
        taskInfoMapper.insert(taskInfo);
        return taskInfo;
    }

    /**
     * 创建定额任务行为
     * @param taskInfo
     * @param tiQutaAddReq
     * @return
     */
    public TaskActionQutaBo createTaskActionQuta(TaskInfo taskInfo, TIQutaAddReq tiQutaAddReq){
        //创建任务行为
        TaskActionQuta taskActionQuta = new TaskActionQuta();
        taskActionQuta.setInfoGuid(taskInfo.getGuid());
        taskActionQuta.setInfoCode(taskInfo.getCode());
        taskActionQuta.setCron(tiQutaAddReq.getCron());
        taskActionQuta.setVolume(tiQutaAddReq.getVolume());
        taskActionQuta.setSuccVol(TaskConstant.ZERO);
        //校验任务行为合法性
        TaskExecAction taskExecAction = getTaskExecAction(tiQutaAddReq.getTaskExecActionGuid());
        taskActionQuta.setExecActionGuid(taskExecAction.getGuid());
        //校验任务运行服务器
        TaskServer taskServer = getTaskServer(tiQutaAddReq.getTaskServerGuid());
        taskActionQuta.setRunServerGuid(taskServer.getGuid());
        taskActionQutaMapper.insert(taskActionQuta);
        TaskActionQutaBo taskActionQutaBo = new TaskActionQutaBo();
        taskActionQutaBo.setTaskActionQuta(taskActionQuta);
        taskActionQutaBo.setTaskExecAction(taskExecAction);
        taskActionQutaBo.setTaskServer(taskServer);
        taskActionQutaBo.setTaskInfo(taskInfo);
        return taskActionQutaBo;
    }

    public TaskActionTimingBo createTaskActionQuta(TaskInfo taskInfo, TITimingAddReq tiTimingAddReq){
        TaskActionTimingBo taskActionTimingBo = new TaskActionTimingBo();
        //创建任务行为
        TaskActionTiming taskActionTiming = new TaskActionTiming();
        taskActionTiming.setInfoGuid(taskInfo.getGuid());
        taskActionTiming.setInfoCode(taskInfo.getCode());
        taskActionTiming.setCron(tiTimingAddReq.getCron());
        //校验任务行为合法性
        TaskExecAction taskExecAction = getTaskExecAction(tiTimingAddReq.getTaskExecActionGuid());
        taskActionTiming.setExecActionGuid(taskExecAction.getGuid());
        //校验任务运行服务器
        TaskServer taskServer = getTaskServer(tiTimingAddReq.getTaskServerGuid());
        taskActionTiming.setRunServerGuid(taskServer.getGuid());
        taskActionTimingMapper.insert(taskActionTiming);
        taskActionTimingBo.setTaskActionTiming(taskActionTiming);
        taskActionTimingBo.setTaskExecAction(taskExecAction);
        taskActionTimingBo.setTaskServer(taskServer);
        taskActionTimingBo.setTaskInfo(taskInfo);
        return taskActionTimingBo;
    }

    /**
     * 输入guid查询任务执行行为
     * 校验结果合法性
     * @param guid
     * @return
     */
    public TaskExecAction getTaskExecAction(Integer guid){
        TaskExecAction taskExecAction = taskExecActionMapper.selectByPrimaryKey(guid);
        //非空
        if(taskExecAction==null){
            throw new BusinessException(ExceptionTypeEnum.TASK_EXEC_ACTION_NO_EXIST);
        }
        return taskExecAction;
    }

    /**
     * 输入guid查询任务运行服务器
     * 校验结果合法性
     * @param guid
     * @return
     */
    public TaskServer getTaskServer(Integer guid){
        TaskServer taskServer = taskServerMapper.selectByPrimaryKey(guid);
        //非空
        if(taskServer==null){
            throw new BusinessException(ExceptionTypeEnum.TASK_SERVER_NO_EXIST);
        }
        return taskServer;
    }

}
