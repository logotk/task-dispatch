package com.allenlogo.task.manager;

import com.allenlogo.task.bo.TaskActionQutaBo;
import com.allenlogo.task.bo.TaskActionTimingBo;
import com.allenlogo.task.bo.TaskBo;
import com.allenlogo.task.constant.TaskConstant;
import com.allenlogo.task.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author allenlogo
 * @Date 2019/1/16
 * @Time 22:35
 * To change this template use File | Settings | File Templates.
 */
@Component
@Slf4j
public class SchedulerManager {

    @Autowired
    private SchedulerFactoryBean schedulerFactoryBean;
    @Autowired
    private OrikaBeanMapper orikaBeanMapper;

    public void initTaskBo(List<TaskBo> taskBoList){
        for(TaskBo taskBo : taskBoList){
            JobKey jobKey = new JobKey(taskBo.getCron(),taskBo.getClassPath());
            createSchedulerTask(taskBo,jobKey);
        }
    }

    public void clear(){
        try {
            schedulerFactoryBean.getScheduler().clear();
        } catch (SchedulerException e) {
            log.error("SchedulerManager -> clear : {}",e);
            throw new BusinessException();
        }
    }

    public void addTaskActionQutaBo(TaskActionQutaBo taskActionQutaBo) {
        TaskBo taskBo = orikaBeanMapper.map(taskActionQutaBo.getTaskInfo(),TaskBo.class);
        taskBo.setCron(taskActionQutaBo.getTaskActionQuta().getCron());
        taskBo.setClassPath(taskActionQutaBo.getTaskExecAction().getClassPath());
        taskBo.setTaskServer(taskActionQutaBo.getTaskServer());
        JobKey jobKey = new JobKey(taskBo.getCron(),taskBo.getClassPath());
        createSchedulerTask(taskBo,jobKey);
    }


    public void addTaskActionTimingBo(TaskActionTimingBo taskActionTimingBo) {
        TaskBo taskBo = orikaBeanMapper.map(taskActionTimingBo.getTaskInfo(),TaskBo.class);
        taskBo.setCron(taskActionTimingBo.getTaskActionTiming().getCron());
        taskBo.setClassPath(taskActionTimingBo.getTaskExecAction().getClassPath());
        taskBo.setTaskServer(taskActionTimingBo.getTaskServer());
        JobKey jobKey = new JobKey(taskBo.getCron(),taskBo.getClassPath());
        createSchedulerTask(taskBo,jobKey);
    }

    public void createSchedulerTask(TaskBo taskBo,JobKey jobKey){
        Scheduler scheduler = schedulerFactoryBean.getScheduler();
        try {
            if(scheduler.getJobDetail(jobKey)==null){
                // 新增
                createSchedulerTask(scheduler,taskBo,taskBo.getClassPath(),jobKey,taskBo.getCron());
            }else {
                // 增加
                addSchedulerTask(scheduler,taskBo.getCron(),jobKey.getGroup(),taskBo);
            }
        } catch (SchedulerException e) {
            log.error("addTaskActionQutaBo -> SchedulerException {}",e);
            throw new BusinessException();
        } catch (Exception e){
            log.error("addTaskActionQutaBo -> Exception {}",e);
            throw new BusinessException();
        }
    }

    public void createSchedulerTask(Scheduler scheduler,TaskBo taskBo,String className,JobKey jobKey,String cron) throws SchedulerException {
        Class<Job> jobClass = null;

        try {
            //实例化具体的Job任务
            jobClass = (Class<Job>) Class.forName(className);
        } catch (ClassNotFoundException e) {
            log.error("createSchedulerTask -> ClassNotFoundException {},{}",className,e);
            throw new BusinessException();
        }
        JobDetail jobDetail = JobBuilder.newJob(jobClass).withIdentity(jobKey).build();
        List<TaskBo> taskBos = Collections.synchronizedList(new ArrayList());
        taskBos.add(taskBo);
        jobDetail.getJobDataMap().put(TaskConstant.JOB_DATA_MAP_TASKBO,taskBos);
        // 基于表达式构建触发器
        CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.cronSchedule(cron);
        // CronTrigger表达式触发器 继承于Trigger
        // TriggerBuilder 用于构建触发器实例
        CronTrigger cronTrigger = TriggerBuilder.newTrigger().withIdentity(cron, jobKey.getGroup())
                .withSchedule(cronScheduleBuilder).build();
        scheduler.scheduleJob(jobDetail, cronTrigger);
    }

    private void addSchedulerTask(Scheduler scheduler,String cron,String gruopName,TaskBo taskBo) throws SchedulerException {
        JobKey jobKey = new JobKey(cron,gruopName);
        JobDetail jobDetail = scheduler.getJobDetail(jobKey);
        List<TaskBo> taskBos = ((List<TaskBo>)jobDetail.getJobDataMap().get(TaskConstant.JOB_DATA_MAP_TASKBO));
        taskBos.add(taskBo);
    }

}

