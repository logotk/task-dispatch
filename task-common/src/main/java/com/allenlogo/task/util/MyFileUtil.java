package com.allenlogo.task.util;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 *
 * @author allenlogo
 * @Date 2019/3/3
 * @Time 21:20
 * To change this template use File | Settings | File Templates.
 */
public class MyFileUtil {
    /**
     * @return
     */
    public static String splicePath(String... path) {
        return String.join(File.separator, path);
    }

    public static void createFile(String path, String contentStr) {
        File scriptsFile = new File(path);
        try {
            FileUtils.write(scriptsFile, contentStr);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String... args) throws IOException, InterruptedException {
        /*System.out.println(splicePath("1","2"));
        createFile(splicePath("F:\\data","test","test.sh"),"exho \"Hello World!\"");
        Properties props=System.getProperties();
        String osName = props.getProperty("os.name");
        System.out.println(osName);*/
        Process process = Runtime.getRuntime().exec("cmd /c dir");
        ProcessInfoReader processInfoReader = ProcessInfoReader
                .getInstance(process);
        processInfoReader.readProcessInfo();
        if (StringUtils.isNotBlank(processInfoReader.getErr())) {
            System.out.println(processInfoReader.getErr());
        }
        System.out.println(processInfoReader.getInfo());
    }
}
