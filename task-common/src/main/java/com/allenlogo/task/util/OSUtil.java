package com.allenlogo.task.util;

import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 *
 * @author allenlogo
 * @Date 2019/3/3
 * @Time 22:04
 * To change this template use File | Settings | File Templates.
 */
public class OSUtil {
    private final static String winKey = "Windows";

    public boolean isWin(){
        Properties props=System.getProperties();
        String osName = props.getProperty("os.name");
        return osName.contains(winKey);
    }
}
