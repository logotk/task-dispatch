/**
 * Copyright ©2015 元光科技 All Rights Reserved 
 *
 */
package com.allenlogo.task.util;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * 
 * 异常工具类
 * Created with IntelliJ IDEA.
 *
 * @author allenlogo
 * @Date 2019/3/3
 * @Time 21:20
 * To change this template use File | Settings | File Templates.
 */
public class ExceptionUtil {

	public static String getExceptionDeatial(Exception e) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		return "<pre>" + sw.toString() + "</pre>";
	}
}
