package com.allenlogo.task.util;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.concurrent.CountDownLatch;

/**
 * 进程信息读取类
 * Created with IntelliJ IDEA.
 *
 * @author allenlogo
 * @Date 2019/3/3
 * @Time 22:04
 * To change this template use File | Settings | File Templates.
 */
@Slf4j
public class ProcessInfoReader {

    private Process process;

    private String info;

    private String err;

    public static ProcessInfoReader getInstance(Process process) {
        ProcessInfoReader processInfoReader = new ProcessInfoReader();
        processInfoReader.process = process;
        return processInfoReader;
    }

    private void readInfos(boolean normal) throws IOException {
        InputStream in = normal ? this.process.getInputStream() : this.process
                .getErrorStream();
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();
        StringBuilder errSb = new StringBuilder();
        try {
            br = new BufferedReader(new InputStreamReader(in, Charset.forName("GBK")));
            String line = null;
            while ((line = br.readLine()) != null) {
                sb.append(line).append("\n");
                if (!normal) {
                    errSb.append(line).append("\n");
                }
            }
            log.info("{},shell result:", (normal ? "normal " : "error "), sb.toString());
        } finally {
            if (br != null) {
                br.close();
            }
        }
        if (normal) {
            this.info = sb.toString();
        } else {
            this.err = errSb.toString();
        }
    }

    public void readProcessInfo() throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(2);
        new Thread(new ReadHandle(this, true, countDownLatch)).start();
        new Thread(new ReadHandle(this, false, countDownLatch)).start();
        countDownLatch.await();
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }

    private static class ReadHandle implements Runnable {
        private ProcessInfoReader processInfoReader;

        private boolean normalFlag = true;

        private CountDownLatch countDownLatch = null;

        public ReadHandle(ProcessInfoReader processInfoReader,
                          boolean normalFlag, CountDownLatch countDownLatch) {
            this.processInfoReader = processInfoReader;
            this.normalFlag = normalFlag;
            this.countDownLatch = countDownLatch;
        }

        @Override
        public void run() {
            try {
                processInfoReader.readInfos(normalFlag);
            } catch (IOException e) {
                e.printStackTrace();
                processInfoReader.err = ExceptionUtil.getExceptionDeatial(e);
            } finally {
                this.countDownLatch.countDown();
            }
        }
    }
}
