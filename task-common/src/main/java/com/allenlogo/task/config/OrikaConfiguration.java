package com.allenlogo.task.config;

import com.allenlogo.task.manager.OrikaBeanMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created with IntelliJ IDEA.
 *
 * @author allenlogo
 * @Date 2019/1/14
 * @Time 20:39
 * To change this template use File | Settings | File Templates.
 */
@Configuration
public class OrikaConfiguration {

    public OrikaConfiguration() {
    }

    @Bean
    public OrikaBeanMapper beanMapper() {
        return new OrikaBeanMapper();
    }
}

