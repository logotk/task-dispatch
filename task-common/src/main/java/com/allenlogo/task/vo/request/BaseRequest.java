package com.allenlogo.task.vo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 基础POST请求类
 */
@Data
@ApiModel("基础请求")
public class BaseRequest {
    @ApiModelProperty("用户guid")
    private Integer userGuid;
}
