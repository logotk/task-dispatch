package com.allenlogo.task.constant;

/**
 * Created with IntelliJ IDEA.
 *
 * @author allenlogo
 * @Date 2019/1/14
 * @Time 20:54
 * To change this template use File | Settings | File Templates.
 */
public class TaskConstant {
    /**
     * 是否删除-否
     */
    public static final Integer IS_DEL_ON = 0;
    /**
     * 是否删除-是
     */
    public static final Integer IS_DEL_OFF = 1;
    /**
     * 是否使用-是
     */
    public static final Integer IS_USE_ON = 0;
    /**
     * 是否使用-否
     */
    public static final Integer IS_USE_OFF = 1;
    /**
     * 管理员guid
     */
    public static final Integer USER_GUID = 1;
    /**
     * 管理员名称
     */
    public static final String USER_NAME = "管理员";
    /**
     * JOB Map Key
     */
    public static final String JOB_DATA_MAP_TASKBO = "taskbo";
    /**
     * 初始值
     */
    public static final Integer ZERO=0;
}
